<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// L
	'logo_spip_description' => 'Affichez facilement le logo de SPIP dans vos articles avec le raccourci &lt;spip|&gt; et dans vos squelettes avec <code>#MODELE{spip}</code> ou <code>#LOGO_SPIP</code>.',
	'logo_spip_slogan'      => 'Affichez facilement le logo de SPIP',
);
